import { LitElement, html, css } from 'lit-element';
import './app/first-component';

export class SftLitElementApp extends LitElement {

  static get properties() {
    return {
      prop: { type: String }
    };
  }

  constructor() {
    super();
    this.prop = 'Hello world';
  }

  static get styles() {
    return css`
      :host {
        display: block;
        box-sizing: border-box;
      }

      *, *:before, *:after {
        box-sizing: inherit;
      }
    `;
  }

  render() {
    return html`
      <h2>${this.prop}<h2>
      <first-component></first-component>
    `;
  }

}

customElements.define('sft-lit-element-app', SftLitElementApp);